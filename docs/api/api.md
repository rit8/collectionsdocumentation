# WorkFlowEngine API
WorkFlowEngine API documentation

## Version: 0.0.1

### /api/v1/configuration/business-logic/entity-type/workflow

#### POST
##### Summary

createActivity

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| workflowEntryConfigurationDTO | body | workflowEntryConfigurationDTO | Yes | [WorkflowEntryConfigurationDTO](#workflowentryconfigurationdto) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [WorkflowEntryConfigurationDTO](#workflowentryconfigurationdto) |
| 201 | Created |  |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /api/v1/deployment/deprecate/entity-type/{entityType}/entity-name/{entityName}

#### POST
##### Summary

deprecateEntity

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| entityName | path | entityName | Yes | string |
| entityType | path | entityType | Yes | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [CommonErrorAPI](#commonerrorapi) |
| 201 | Created |  |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /api/v1/deployment/deprecate/entity-type/{entityType}/entity-name/{entityName}/version/{version}

#### POST
##### Summary

deprecateByVersion

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| entityName | path | entityName | Yes | string |
| entityType | path | entityType | Yes | string |
| version | path | version | Yes | integer |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [CommonErrorAPI](#commonerrorapi) |
| 201 | Created |  |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /api/v1/deployment/entity-type/{entityType}

#### GET
##### Summary

getByEntityType

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| entityType | path | entityType | Yes | string |
| page | query | Page number of the requested page | No | integer |
| size | query | Size of a page | No | integer |
| sort | query | Sorting criteria in the format: property(,asc\|desc). Default sort order is ascending. Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [ [EntityConfigurationDTO](#entityconfigurationdto) ] |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

#### POST
##### Summary

create

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| entityConfigurationDTO | body | entityConfigurationDTO | Yes | [EntityConfigurationDTO](#entityconfigurationdto) |
| entityType | path | entityType | Yes | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [EntityConfigurationDTO](#entityconfigurationdto) |
| 201 | Created |  |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /api/v1/deployment/entity-type/{entityType}/entity-name/{entityName}

#### GET
##### Summary

getActiveByEntityTypeAndEntityName

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| entityName | path | entityName | Yes | string |
| entityType | path | entityType | Yes | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [EntityConfigurationDTO](#entityconfigurationdto) |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /api/v1/scheduler/housekeeping

#### POST
##### Summary

schedulerApiCall

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| schedulerApiDto | body | schedulerApiDto | Yes | [SchedulerApiDto](#schedulerapidto) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [ResponseEntity](#responseentity) |
| 201 | Created |  |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /api/v1/scheduler/reject

#### POST
##### Summary

schedulerApiCall

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| schedulerApiDto | body | schedulerApiDto | Yes | [SchedulerApiDto](#schedulerapidto) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [ResponseEntity](#responseentity) |
| 201 | Created |  |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /api/v1/scheduler/retry

#### POST
##### Summary

schedulerApiCall

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| schedulerApiDto | body | schedulerApiDto | Yes | [SchedulerApiDto](#schedulerapidto) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [ResponseEntity](#responseentity) |
| 201 | Created |  |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /v1/execution/workflow

#### GET
##### Summary

getAllWorkflow

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| cleanDate.equals | query |  | No | dateTime |
| cleanDate.greaterThan | query |  | No | dateTime |
| cleanDate.greaterThanOrEqual | query |  | No | dateTime |
| cleanDate.in[0].epochSecond | query |  | No | long |
| cleanDate.in[0].nano | query |  | No | integer |
| cleanDate.lessThan | query |  | No | dateTime |
| cleanDate.lessThanOrEqual | query |  | No | dateTime |
| cleanDate.specified | query |  | No | boolean |
| context.contains | query |  | No | string |
| context.equals | query |  | No | string |
| context.in | query |  | No | [ string ] |
| context.specified | query |  | No | boolean |
| createTime.equals | query |  | No | dateTime |
| createTime.greaterThan | query |  | No | dateTime |
| createTime.greaterThanOrEqual | query |  | No | dateTime |
| createTime.in[0].epochSecond | query |  | No | long |
| createTime.in[0].nano | query |  | No | integer |
| createTime.lessThan | query |  | No | dateTime |
| createTime.lessThanOrEqual | query |  | No | dateTime |
| createTime.specified | query |  | No | boolean |
| createdDate.equals | query |  | No | dateTime |
| createdDate.greaterThan | query |  | No | dateTime |
| createdDate.greaterThanOrEqual | query |  | No | dateTime |
| createdDate.in[0].epochSecond | query |  | No | long |
| createdDate.in[0].nano | query |  | No | integer |
| createdDate.lessThan | query |  | No | dateTime |
| createdDate.lessThanOrEqual | query |  | No | dateTime |
| createdDate.specified | query |  | No | boolean |
| currentTask.equals | query |  | No | integer |
| currentTask.greaterThan | query |  | No | integer |
| currentTask.greaterThanOrEqual | query |  | No | integer |
| currentTask.in | query |  | No | [ integer ] |
| currentTask.lessThan | query |  | No | integer |
| currentTask.lessThanOrEqual | query |  | No | integer |
| currentTask.specified | query |  | No | boolean |
| endTime.equals | query |  | No | dateTime |
| endTime.greaterThan | query |  | No | dateTime |
| endTime.greaterThanOrEqual | query |  | No | dateTime |
| endTime.in[0].epochSecond | query |  | No | long |
| endTime.in[0].nano | query |  | No | integer |
| endTime.lessThan | query |  | No | dateTime |
| endTime.lessThanOrEqual | query |  | No | dateTime |
| endTime.specified | query |  | No | boolean |
| error.contains | query |  | No | string |
| error.equals | query |  | No | string |
| error.in | query |  | No | [ string ] |
| error.specified | query |  | No | boolean |
| executionTrace.contains | query |  | No | string |
| executionTrace.equals | query |  | No | string |
| executionTrace.in | query |  | No | [ string ] |
| executionTrace.specified | query |  | No | boolean |
| id.equals | query |  | No | long |
| id.greaterThan | query |  | No | long |
| id.greaterThanOrEqual | query |  | No | long |
| id.in | query |  | No | [ long ] |
| id.lessThan | query |  | No | long |
| id.lessThanOrEqual | query |  | No | long |
| id.specified | query |  | No | boolean |
| inputs.contains | query |  | No | string |
| inputs.equals | query |  | No | string |
| inputs.in | query |  | No | [ string ] |
| inputs.specified | query |  | No | boolean |
| label.contains | query |  | No | string |
| label.equals | query |  | No | string |
| label.in | query |  | No | [ string ] |
| label.specified | query |  | No | boolean |
| lastModifiedDate.equals | query |  | No | dateTime |
| lastModifiedDate.greaterThan | query |  | No | dateTime |
| lastModifiedDate.greaterThanOrEqual | query |  | No | dateTime |
| lastModifiedDate.in[0].epochSecond | query |  | No | long |
| lastModifiedDate.in[0].nano | query |  | No | integer |
| lastModifiedDate.lessThan | query |  | No | dateTime |
| lastModifiedDate.lessThanOrEqual | query |  | No | dateTime |
| lastModifiedDate.specified | query |  | No | boolean |
| name.contains | query |  | No | string |
| name.equals | query |  | No | string |
| name.in | query |  | No | [ string ] |
| name.specified | query |  | No | boolean |
| outputs.contains | query |  | No | string |
| outputs.equals | query |  | No | string |
| outputs.in | query |  | No | [ string ] |
| outputs.specified | query |  | No | boolean |
| page | query | Page number of the requested page | No | integer |
| parentTaskExecutionId.contains | query |  | No | string |
| parentTaskExecutionId.equals | query |  | No | string |
| parentTaskExecutionId.in | query |  | No | [ string ] |
| parentTaskExecutionId.specified | query |  | No | boolean |
| priority.equals | query |  | No | integer |
| priority.greaterThan | query |  | No | integer |
| priority.greaterThanOrEqual | query |  | No | integer |
| priority.in | query |  | No | [ integer ] |
| priority.lessThan | query |  | No | integer |
| priority.lessThanOrEqual | query |  | No | integer |
| priority.specified | query |  | No | boolean |
| size | query | Size of a page | No | integer |
| sort | query | Sorting criteria in the format: property(,asc\|desc). Default sort order is ascending. Multiple sort criteria are supported. | No | [ string ] |
| startTime.equals | query |  | No | dateTime |
| startTime.greaterThan | query |  | No | dateTime |
| startTime.greaterThanOrEqual | query |  | No | dateTime |
| startTime.in[0].epochSecond | query |  | No | long |
| startTime.in[0].nano | query |  | No | integer |
| startTime.lessThan | query |  | No | dateTime |
| startTime.lessThanOrEqual | query |  | No | dateTime |
| startTime.specified | query |  | No | boolean |
| status.contains | query |  | No | string |
| status.equals | query |  | No | string |
| status.in | query |  | No | [ string ] |
| status.specified | query |  | No | boolean |
| tags.contains | query |  | No | string |
| tags.equals | query |  | No | string |
| tags.in | query |  | No | [ string ] |
| tags.specified | query |  | No | boolean |
| webhooks.contains | query |  | No | string |
| webhooks.equals | query |  | No | string |
| webhooks.in | query |  | No | [ string ] |
| webhooks.specified | query |  | No | boolean |
| workflowDefinition.contains | query |  | No | string |
| workflowDefinition.equals | query |  | No | string |
| workflowDefinition.in | query |  | No | [ string ] |
| workflowDefinition.specified | query |  | No | boolean |
| workflowId.contains | query |  | No | string |
| workflowId.equals | query |  | No | string |
| workflowId.in | query |  | No | [ string ] |
| workflowId.specified | query |  | No | boolean |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [ [WorkflowExecution](#workflowexecution) ] |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /v1/execution/workflow/count

#### GET
##### Summary

countEntityWorkflowExecutions

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| cleanDate.equals | query |  | No | dateTime |
| cleanDate.greaterThan | query |  | No | dateTime |
| cleanDate.greaterThanOrEqual | query |  | No | dateTime |
| cleanDate.in[0].epochSecond | query |  | No | long |
| cleanDate.in[0].nano | query |  | No | integer |
| cleanDate.lessThan | query |  | No | dateTime |
| cleanDate.lessThanOrEqual | query |  | No | dateTime |
| cleanDate.specified | query |  | No | boolean |
| context.contains | query |  | No | string |
| context.equals | query |  | No | string |
| context.in | query |  | No | [ string ] |
| context.specified | query |  | No | boolean |
| createTime.equals | query |  | No | dateTime |
| createTime.greaterThan | query |  | No | dateTime |
| createTime.greaterThanOrEqual | query |  | No | dateTime |
| createTime.in[0].epochSecond | query |  | No | long |
| createTime.in[0].nano | query |  | No | integer |
| createTime.lessThan | query |  | No | dateTime |
| createTime.lessThanOrEqual | query |  | No | dateTime |
| createTime.specified | query |  | No | boolean |
| createdDate.equals | query |  | No | dateTime |
| createdDate.greaterThan | query |  | No | dateTime |
| createdDate.greaterThanOrEqual | query |  | No | dateTime |
| createdDate.in[0].epochSecond | query |  | No | long |
| createdDate.in[0].nano | query |  | No | integer |
| createdDate.lessThan | query |  | No | dateTime |
| createdDate.lessThanOrEqual | query |  | No | dateTime |
| createdDate.specified | query |  | No | boolean |
| currentTask.equals | query |  | No | integer |
| currentTask.greaterThan | query |  | No | integer |
| currentTask.greaterThanOrEqual | query |  | No | integer |
| currentTask.in | query |  | No | [ integer ] |
| currentTask.lessThan | query |  | No | integer |
| currentTask.lessThanOrEqual | query |  | No | integer |
| currentTask.specified | query |  | No | boolean |
| endTime.equals | query |  | No | dateTime |
| endTime.greaterThan | query |  | No | dateTime |
| endTime.greaterThanOrEqual | query |  | No | dateTime |
| endTime.in[0].epochSecond | query |  | No | long |
| endTime.in[0].nano | query |  | No | integer |
| endTime.lessThan | query |  | No | dateTime |
| endTime.lessThanOrEqual | query |  | No | dateTime |
| endTime.specified | query |  | No | boolean |
| error.contains | query |  | No | string |
| error.equals | query |  | No | string |
| error.in | query |  | No | [ string ] |
| error.specified | query |  | No | boolean |
| executionTrace.contains | query |  | No | string |
| executionTrace.equals | query |  | No | string |
| executionTrace.in | query |  | No | [ string ] |
| executionTrace.specified | query |  | No | boolean |
| id.equals | query |  | No | long |
| id.greaterThan | query |  | No | long |
| id.greaterThanOrEqual | query |  | No | long |
| id.in | query |  | No | [ long ] |
| id.lessThan | query |  | No | long |
| id.lessThanOrEqual | query |  | No | long |
| id.specified | query |  | No | boolean |
| inputs.contains | query |  | No | string |
| inputs.equals | query |  | No | string |
| inputs.in | query |  | No | [ string ] |
| inputs.specified | query |  | No | boolean |
| label.contains | query |  | No | string |
| label.equals | query |  | No | string |
| label.in | query |  | No | [ string ] |
| label.specified | query |  | No | boolean |
| lastModifiedDate.equals | query |  | No | dateTime |
| lastModifiedDate.greaterThan | query |  | No | dateTime |
| lastModifiedDate.greaterThanOrEqual | query |  | No | dateTime |
| lastModifiedDate.in[0].epochSecond | query |  | No | long |
| lastModifiedDate.in[0].nano | query |  | No | integer |
| lastModifiedDate.lessThan | query |  | No | dateTime |
| lastModifiedDate.lessThanOrEqual | query |  | No | dateTime |
| lastModifiedDate.specified | query |  | No | boolean |
| name.contains | query |  | No | string |
| name.equals | query |  | No | string |
| name.in | query |  | No | [ string ] |
| name.specified | query |  | No | boolean |
| outputs.contains | query |  | No | string |
| outputs.equals | query |  | No | string |
| outputs.in | query |  | No | [ string ] |
| outputs.specified | query |  | No | boolean |
| parentTaskExecutionId.contains | query |  | No | string |
| parentTaskExecutionId.equals | query |  | No | string |
| parentTaskExecutionId.in | query |  | No | [ string ] |
| parentTaskExecutionId.specified | query |  | No | boolean |
| priority.equals | query |  | No | integer |
| priority.greaterThan | query |  | No | integer |
| priority.greaterThanOrEqual | query |  | No | integer |
| priority.in | query |  | No | [ integer ] |
| priority.lessThan | query |  | No | integer |
| priority.lessThanOrEqual | query |  | No | integer |
| priority.specified | query |  | No | boolean |
| startTime.equals | query |  | No | dateTime |
| startTime.greaterThan | query |  | No | dateTime |
| startTime.greaterThanOrEqual | query |  | No | dateTime |
| startTime.in[0].epochSecond | query |  | No | long |
| startTime.in[0].nano | query |  | No | integer |
| startTime.lessThan | query |  | No | dateTime |
| startTime.lessThanOrEqual | query |  | No | dateTime |
| startTime.specified | query |  | No | boolean |
| status.contains | query |  | No | string |
| status.equals | query |  | No | string |
| status.in | query |  | No | [ string ] |
| status.specified | query |  | No | boolean |
| tags.contains | query |  | No | string |
| tags.equals | query |  | No | string |
| tags.in | query |  | No | [ string ] |
| tags.specified | query |  | No | boolean |
| webhooks.contains | query |  | No | string |
| webhooks.equals | query |  | No | string |
| webhooks.in | query |  | No | [ string ] |
| webhooks.specified | query |  | No | boolean |
| workflowDefinition.contains | query |  | No | string |
| workflowDefinition.equals | query |  | No | string |
| workflowDefinition.in | query |  | No | [ string ] |
| workflowDefinition.specified | query |  | No | boolean |
| workflowId.contains | query |  | No | string |
| workflowId.equals | query |  | No | string |
| workflowId.in | query |  | No | [ string ] |
| workflowId.specified | query |  | No | boolean |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | long |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /v1/execution/workflow/workflowid/{id}

#### GET
##### Summary

getWorkflowById

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | id | Yes | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | object |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /v1/execution/workflow/{id}

#### GET
##### Summary

getWorkflow

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | id | Yes | long |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | object |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

#### POST
##### Summary

createWorkflow

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | id | Yes | string |
| workflow | body | workflow | Yes | object |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | object |
| 201 | Created |  |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /v1/execution/workflow/{id}/reject

#### POST
##### Summary

rejectWorkflow

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | body | Yes | object |
| id | path | id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK |
| 201 | Created |
| 401 | Unauthorized |
| 403 | Forbidden |
| 404 | Not Found |

### /v1/execution/workflow/{id}/resubmit

#### GET
##### Summary

resubmit

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | body | Yes | object |
| id | path | id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK |
| 401 | Unauthorized |
| 403 | Forbidden |
| 404 | Not Found |

### /v1/execution/workflow/{id}/retry

#### POST
##### Summary

retryWorkflow

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | body | Yes | object |
| id | path | id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK |
| 201 | Created |
| 401 | Unauthorized |
| 403 | Forbidden |
| 404 | Not Found |

### /v1/execution/workflow/{id}/run

#### POST
##### Summary

runWorkflow

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | id | Yes | string |
| workflow | body | workflow | Yes | object |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | object |
| 201 | Created |  |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### /v1/execution/workflow/{id}/skiptask/{taskId}

#### POST
##### Summary

skipTaskWorkflow

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | id | Yes | string |
| taskId | path | taskId | Yes | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | string |
| 201 | Created |  |
| 401 | Unauthorized |  |
| 403 | Forbidden |  |
| 404 | Not Found |  |

### Models

#### CommonErrorAPI

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| code | string |  | No |
| message | string |  | No |
| reason | string |  | No |
| reference | string |  | No |

#### ConfigurationDetailsDTO

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| activationDate | dateTime |  | No |
| active | boolean |  | Yes |
| deprecated | boolean |  | No |
| description | string |  | No |
| entityName | string |  | No |
| entityType | string |  | No |
| schemaVersion | integer |  | Yes |
| user | string |  | No |
| version | integer |  | Yes |

#### EntityConfigurationDTO

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| activationDate | dateTime |  | Yes |
| active | boolean |  | Yes |
| description | string |  | No |
| entityName | string |  | Yes |
| entityType | string |  | Yes |
| payload | string |  | Yes |
| schemaVersion | integer |  | Yes |
| user | string |  | No |
| version | integer |  | Yes |

#### Error

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| message | string |  | No |
| stackTrace | [ string ] |  | No |
| taskContext | [TaskExecution](#taskexecution) |  | No |

#### Getter

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| map | object |  | No |

#### HashMapOfstringAndobject

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| HashMapOfstringAndobject | object |  |  |

#### MapObject

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| MapObject | object |  |  |

#### MapOfstringAndobject

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| MapOfstringAndobject | object |  |  |

#### ResponseEntity

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| body | object |  | No |
| statusCode | string | _Enum:_ `"100 CONTINUE"`, `"101 SWITCHING_PROTOCOLS"`, `"102 PROCESSING"`, `"103 CHECKPOINT"`, `"200 OK"`, `"201 CREATED"`, `"202 ACCEPTED"`, `"203 NON_AUTHORITATIVE_INFORMATION"`, `"204 NO_CONTENT"`, `"205 RESET_CONTENT"`, `"206 PARTIAL_CONTENT"`, `"207 MULTI_STATUS"`, `"208 ALREADY_REPORTED"`, `"226 IM_USED"`, `"300 MULTIPLE_CHOICES"`, `"301 MOVED_PERMANENTLY"`, `"302 FOUND"`, `"302 MOVED_TEMPORARILY"`, `"303 SEE_OTHER"`, `"304 NOT_MODIFIED"`, `"305 USE_PROXY"`, `"307 TEMPORARY_REDIRECT"`, `"308 PERMANENT_REDIRECT"`, `"400 BAD_REQUEST"`, `"401 UNAUTHORIZED"`, `"402 PAYMENT_REQUIRED"`, `"403 FORBIDDEN"`, `"404 NOT_FOUND"`, `"405 METHOD_NOT_ALLOWED"`, `"406 NOT_ACCEPTABLE"`, `"407 PROXY_AUTHENTICATION_REQUIRED"`, `"408 REQUEST_TIMEOUT"`, `"409 CONFLICT"`, `"410 GONE"`, `"411 LENGTH_REQUIRED"`, `"412 PRECONDITION_FAILED"`, `"413 PAYLOAD_TOO_LARGE"`, `"413 REQUEST_ENTITY_TOO_LARGE"`, `"414 URI_TOO_LONG"`, `"414 REQUEST_URI_TOO_LONG"`, `"415 UNSUPPORTED_MEDIA_TYPE"`, `"416 REQUESTED_RANGE_NOT_SATISFIABLE"`, `"417 EXPECTATION_FAILED"`, `"418 I_AM_A_TEAPOT"`, `"419 INSUFFICIENT_SPACE_ON_RESOURCE"`, `"420 METHOD_FAILURE"`, `"421 DESTINATION_LOCKED"`, `"422 UNPROCESSABLE_ENTITY"`, `"423 LOCKED"`, `"424 FAILED_DEPENDENCY"`, `"426 UPGRADE_REQUIRED"`, `"428 PRECONDITION_REQUIRED"`, `"429 TOO_MANY_REQUESTS"`, `"431 REQUEST_HEADER_FIELDS_TOO_LARGE"`, `"451 UNAVAILABLE_FOR_LEGAL_REASONS"`, `"500 INTERNAL_SERVER_ERROR"`, `"501 NOT_IMPLEMENTED"`, `"502 BAD_GATEWAY"`, `"503 SERVICE_UNAVAILABLE"`, `"504 GATEWAY_TIMEOUT"`, `"505 HTTP_VERSION_NOT_SUPPORTED"`, `"506 VARIANT_ALSO_NEGOTIATES"`, `"507 INSUFFICIENT_STORAGE"`, `"508 LOOP_DETECTED"`, `"509 BANDWIDTH_LIMIT_EXCEEDED"`, `"510 NOT_EXTENDED"`, `"511 NETWORK_AUTHENTICATION_REQUIRED"` | No |
| statusCodeValue | integer |  | No |

#### SchedulerApiDto

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| schedulerName | string |  | No |
| schedulerParameters | object |  | No |
| transactionId | string |  | No |

#### TaskExecution

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| action | string |  | No |
| active | boolean |  | No |
| after | [ [MapObject](#mapobject) ] |  | No |
| before | [ [MapObject](#mapobject) ] |  | No |
| cleanDate | dateTime |  | No |
| createTime | dateTime |  | No |
| endTime | dateTime |  | No |
| error | [Error](#error) |  | No |
| exception | [ [MapObject](#mapobject) ] |  | No |
| execution | object |  | No |
| executionTime | long |  | No |
| failureInputParameters | object |  | No |
| failureTransactionId | string |  | No |
| failureWorkflowId | string |  | No |
| finalize | [ [MapObject](#mapobject) ] |  | No |
| id | long |  | No |
| jobId | string |  | No |
| label | string |  | No |
| map | object |  | No |
| name | string |  | No |
| output | object |  | No |
| parentId | string |  | No |
| parentType | string |  | No |
| priority | integer |  | No |
| progress | integer |  | No |
| retry | integer |  | No |
| retryAttempts | integer |  | No |
| retryDelay | long |  | No |
| retryDelayFactor | integer |  | No |
| retryDelayMillis | long |  | No |
| startTime | dateTime |  | No |
| status | string | _Enum:_ `"CREATED"`, `"STARTED"`, `"FAILED"`, `"CANCELLED"`, `"SKIPPED"`, `"COMPLETED"`, `"NOT_ACTIVE"` | No |
| taskId | string |  | No |
| taskNumber | integer |  | No |
| timeout | string |  | No |
| type | string |  | No |
| workflowId | string |  | No |

#### WorkflowConfigurationDTO

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| defaultAction | string |  | No |
| executionType | string |  | No |
| failureWorkflowId | string |  | No |
| input | [ [Getter](#getter) ] |  | No |
| name | string |  | No |
| outputParameters | object |  | No |
| pullContext | object |  | No |
| pushContext | object |  | No |
| restartable | boolean |  | No |
| tasks | [ [MapOfstringAndobject](#mapofstringandobject) ] |  | No |
| webhooks | [ [HashMapOfstringAndobject](#hashmapofstringandobject) ] |  | No |
| workflowId | string |  | No |
| workflowStatusListenerEnabled | boolean |  | No |

#### WorkflowEntryConfigurationDTO

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| configurationDetails | [ConfigurationDetailsDTO](#configurationdetailsdto) |  | No |
| workflowConfiguration | [WorkflowConfigurationDTO](#workflowconfigurationdto) |  | No |

#### WorkflowExecution

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| WorkflowExecution | object |  |  |
