
## Architecture

Workflow Engine was build using the principles of hexagonal architecture ( Ports & Adapters Pattern).

![hexagonal](img/workflowengine.achitecture1.png)


The hexagonal architecture is based on three principles and techniques:

* Explicitly separate Application, Domain, and Infrastructure
* Dependencies are going from Application and Infrastructure to the Domain
* We isolate the boundaries by using Ports and Adapters


![hexagonal](img/workflowengine.achitecture2.png)




**On the left, the Application side**

This is the side through which the user or external programs will interact with the application. It contains the code that allows these interactions. Typically, your user interface code, your HTTP routes for an API, your JSON serializations to programs that consume your application are here.

**The Domain, in the center**

This is the part that we want to isolate from both left and right sides. It contains all the code that concerns and implements business logic. The business vocabulary and the pure business logic, which relates to the concrete problem that solves your application, everything that makes it rich and specific is at the center.

**On the right, the Infrastructure side**

This is where we’ll find what your application needs, what it drives to work. It contains essential infrastructure details such as the code that interacts with your database, makes calls to the file system, or code that handles HTTP calls to other applications on which you depend for example.

 
![hexagonal](img/hexagone.png)
This architecture is the hexagon, as we see on the previous figure. Why a hexagon? The main reason is that it is an easy-to-draw shape that leaves room to represent multiple ports and adapters on the diagram. And it turns out that even if the hexagon is quite anecdotal in the end, the expression Hexagonal Architecture is more popular than Ports & Adapters Pattern.

### Goals in using this architecture
* Separates problems
* Put business logic at the forefront of code
    * Domain logic is the core
* Inversion of dependency
* Facilitate development
    * The db is a detail
    * The web is a detail
* Facilitate deployment
* Facilitate maintenance
* Keep frameworks at arm's length
* Keep options open

#### Dependencies inversion on the right

![hexagonal](img/no_interface.png)

The interface allows to reverse the direction of this dependency:

![hexagonal](img/interface.png)

## Implementation

![hexagonal](img/app_domain_infra.png)

## Why not use Layers

- Web
- Business
- Persistence

![hexagonal](img/arch_layer.png)


We have been using layer with success, but we got into some issue, that this being a product we feel that this is a more clean approach

### Pitfalls in using layers
* Database-driven design - hard to change things due to coupling to the database (using DTO minimizes this, but still a issue )
* Blurred boundaries - easily blurred boundaries between components.
* Hard-to-test shortcuts - hard to test due to many dependencies
* Hidden functionality 
    - Hard to find the use cases in the code
    - Hard to parallelize work on the same feature




