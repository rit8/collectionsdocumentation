# Workflow Engine Configuration

| Name | Description | Type | Default value
-----| ----------- | ---- | ------------- |
logging.level.ROOT| Define the logging level for logs from ROOT | string | INFO |
logging.level.io.github.jhipster| Define the logging level for logs from jHipster | string  | INFO |
logging.level.com.readinessit.workflowengine| Define the logging level for logs for the adaptor | string  | INFO |
app.hazelcast.config.instanceName | Instance name of hazelcast | string | msentitytransactions
app.hazelcast.config.port | Port value of hazelcast | Integer | 5701
app.hazelcast.config.kubernetes.enabled | On/off switch for kubernetes hazelcast | boolean | false
app.hazelcast.config.kubernetes.namespace | Kubernetes hazelcast namespace | string |
app.hazelcast.config.kubernetes.serviceName | Kubernetes hazelcast service name | string |
restTemplate.logging.intercept.enabled | Enable or disable inbound and outbound logging for rest calls | boolean | true
restTemplate.factory.readTimeout | Timeout value for reading http calls | Long | 5000
restTemplate.factory.connectTimeout | Timeout value for connecting to server for http calls | Long | 3000
restTemplate.httpClient.maxConnTotal | Total maximum connections available | Long | 100
restTemplate.httpClient.maxConnPerRoute | Maximum connections per rout | Long | 10
workflowengine.scheduler.retry.lessTimeValue| Time value since the last Execution to allow the retry of an execution| Integer | 1
workflowengine.scheduler.retry.lessTimeUnits| Time units since the last Execution to allow the retry of an execution| Integer | 1
workflowengine.scheduler.retry.mode| Mode used to execute the retries if absent will be used the local mode | String | local
workflowengine.scheduler.retry.http.url| url used by the http mode of retries | String | local
workflowengine.retries.maxDelay| Max delay that can be used in the internal retries of the execution, value is interpreted in milliseconds | Long | 60000
workflowengine.messenger.queuesPrefix | Messenger queues prefix  used to better identify different workflows messaging in the same external messenger | String | WFW_
workflowengine.messenger.provider.jms.active | Activate the jsm messenger provider | boolean | false
workflowengine.messenger.provider.sync.active | Activate the sync messenger provider | boolean | true
workflowengine.messenger.kafka.active | Activate the kafka messenger provider | boolean | false |
workflowengine.messenger.kafka.bootstrapAddress | kakfa messenger bootstrap Address| String |
workflowengine.messenger.kafka.numPartitions | kakfa messenger partitions number| String | 1
workflowengine.messenger.kafka.replicationFactor | kakfa messenger replication Factor | String | 1
workflowengine.repository.workflowdefinition.provider | Choose the provider used for the configurations of the workflow| String | jpa
workflowengine.repository.context.ttl | Time to live of the context stored in the repository | Long | 60000
workflowengine.repository.context.provider | Choose the provider used for the context of the workflow | String | memory
workflowengine.repository.workflowexecution.provider | Choose the provider used for the workflow_execution of the workflow | String | jpa
workflowengine.repository.workflowexecution.ttl | Time to live of the workflow_execution stored in the repository | Long | 60000
workflowengine.repository.taskexecution.provider | Choose the provider used for the task_execution of the workflow | String | jpa
workflowengine.repository.taskexecution.ttl | Time to live of the task_execution stored in the repository | Long | 60000
services.hazelcast.maxSizePolicy | Policy to clean the Hazelcast MapStores by sizing | String |
services.contextService.mapConfig.name | Hazelcast context MapStore name | String |
services.contextService.mapConfig.maxSize | Hazelcast context MapStore maxsize. Should be given in accord with the policy | Long |
services.contextService.mapConfig.backupCount | Hazelcast context MapStore backup count | Integer |
services.contextService.mapConfig.timeToLiveSeconds | Hazelcast context MapStore entry time to live in seconds | Long |
services.contextService.mapConfig.mapstore.writeDelaySeconds | Hazelcast context MapStore time to write in the database after the last change. If value is 0 the write will not be async| Long |
services.workflowExecutionService.mapConfig.name | Hazelcast WorkflowExecution MapStore name | String |
services.workflowExecutionService.mapConfig.active | Hazelcast WorkflowExecution MapStore activation | Boolean |
services.workflowExecutionService.mapConfig.maxSize | Hazelcast WorkflowExecution MapStore maxsize. Should be given in accord with the policy | Long |
services.workflowExecutionService.mapConfig.backupCount | Hazelcast WorkflowExecution MapStore backup count | Integer |
services.workflowExecutionService.mapConfig.timeToLiveSeconds | Hazelcast WorkflowExecution MapStore entry time to live in seconds | Long |
services.workflowExecutionService.mapConfig.mapstore.writeDelaySeconds | Hazelcast WorkflowExecutionMapStore time to write in the database after the last change. If value is 0 the write will not be async| Long |
services.taskExecutionService.mapConfig.name | Hazelcast TaskExecution MapStore name | String |
services.taskExecutionService.mapConfig.active | Hazelcast TaskExecution MapStore activation | Boolean |
services.taskExecutionService.mapConfig.maxSize | Hazelcast TaskExecution MapStore maxsize. Should be given in accord with the policy | Long |
services.taskExecutionService.mapConfig.backupCount | Hazelcast TaskExecution MapStore backup count | Integer |
services.taskExecutionService.mapConfig.timeToLiveSeconds | Hazelcast TaskExecution MapStore entry time to live in seconds | Long |
services.taskExecutionService.mapConfig.mapstore.writeDelaySeconds | Hazelcast TaskExecution MapStore time to write in the database after the last change. If value is 0 the write will not be async| Long |

