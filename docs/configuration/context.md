## Context

Store and retrieve information between flows, this information is keep in memory (local or replicated cache with ttl, depending on the implementation)

`**NOTE**: workflow engine has api's that can get information from any task, this is to be used only in situations that needed high performace` 


     "pullContext": {
        	"stackId": "${inputs.workflow.token}"
        },
        
        
    "pushContext": {
        "stackId": "${execution.transbankadaptor.output.token}",
        "ttl": "84600",
        "stackParameters": {
            "inputs": "${inputs}",
            "token":"${execution.transbankadaptor.output.token}"
        }
      },  
