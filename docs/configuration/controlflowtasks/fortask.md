# For task

Run a set of task in a for until rotate all members of the list

## Task Definition Parameters

Parameter                   | Required    | Type    |        Description                                           | Notes
----------------------------| ------------|---------|--------------------------------------------------------------|-------- 
label                       | yes         | String  | unique key                                                   |
type                        | yes         | String  | terminate type task                                          | must be for
list                        | yes         | String  | spel expresion to be substitute for an list                  |
tasks                       | yes         | List    | expression to determine witch branch will be executed        |

## execution

Inside the for execution the task of each loop can get access to the tasks inside the same loop and to the member of
list that is being currently being processed.

Parameter                   |Type     | example
----------------------------|---------|-------------
listMember                  | Object  | ${listMember} -> this will substitute the expression with the member used on the iteration
subExecution                | Map     | ${subexecution.task1.output} -> this will permit to one task to access to the output of the task1 on the the right iteration

## Example

    {
        "label": "forTask",
        "type": "for",
        "list": "${workflow.list}",
        "tasks": [
           {
                "label": "task1",
                "component": "Http",
                "type": "simple",
                "description": "test",
                "inputParameters": {
                    "url": "http://transbankadaptor-app:8283/api/web-pay-mall/init-transaction",
                    "headers": {
                        "Content-Type": "application/json"
                    },
                    "body": {
                        "buyOrder": "${listMember}"
                    }
                }
            },
            {
                "label": "task2",
                "type": "simple",
                "component": "Print",        
                "text": "${subexecution.task1.output.body.code}"
            }
        ]
    }
