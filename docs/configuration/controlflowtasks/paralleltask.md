# Parallel tasks

Run the task's collection in parallel, without waiting until the previous function to be completed.

## Task Definition Parameters

Parameter                   | Required    | Type    |        Description                                           | Notes
----------------------------| ------------|---------|--------------------------------------------------------------|-------- 
label                       | yes         | String  | unique key                                                   |
type                        | yes         | String  | logical type task                                            | must be parallel
tasks                       | yes         | List    | List of tasks to be executed                                 |

## Example

    {
        "schemaVersion": 1,
        "version": 1,
        "restartable": false,
        "name": "p2",
        "description": "p2",
        "executionType": "sync",
        "workflowStatusListenerEnabled": false,
        "defaultAction": "TERMINATE",
        "workflowId": "p2",
        "tasks": [
              {
                    "type": "simple",
                    "name": "Print",
                    "label": "task1",
                    "text": "hello world"
                },
                {
                    "type": "parallel",
                    "name": "parallel",
                    "label": "parallel",
                    "tasks": [
                                {
                            "type": "simple",
                            "name": "Print",
                            "label": "task3",
                            "text": "hello2"
                        },
                        {
                            "type": "simple",
                            "name": "Print",
                            "label": "task4",
                            "text": "hello3"
                        }
                        
                        ]
                
                }
            
            
        ]
    }
