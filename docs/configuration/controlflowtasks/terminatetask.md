#Terminate task

A terminate task can interrupt the execution flow, it can force terminate an execution if a condition is not meet.
**If the expression on the task definition is true then it terminates**
Terminate type task is simpler version of logical task, it only has one option, if expression is true then it will terminate, if false it will continue.

The flow will terminate and be stored as COMPLETED.

![Terminate](../../img/wf_terminate.png)


##Task Definition Parameters 

Parameter                   | Required    | Type    |        Description                                           | Notes
----------------------------| ------------|---------|--------------------------------------------------------------|-------- 
label                       | yes         | String  | unique key                                                   | 
type                        | yes         | String  | terminate type task                                          | must be terminate 
expression                  | yes         | String  | logical expression to be validate                            |
output                      | no          | Map     | return parameters, this can be used to give a error message to the api | 


##Example 1

the output is a dynamic object, it will depend on of the type of flow.
when using in sync mode, and the client expect a response then on the output we can use "htttReponse" and the api will response will that.

    {
        "label": "validateIsValid",
        "name": "validateIsValid",
        "type": "terminate",
        "description": "validateIsValid",
        "expression": "${execution.getAsset.output.code!= '0'}",        
        "output": {
            "httpResponse":
                {
                    "statusCode": 400,
                    "body":
                        {
                            "erro": "Bad Request",
                            "message": "....."
                        }                
                }
            }
    }

##Example 2    

if we don't expect a response, then we can use the output to store any information we need to analise and in a query's to the system.
    
        {
            "label": "validateIsValid",
            "name": "validateIsValid",
            "type": "terminate",
            "description": "validateIsValid",
            "expression": "${execution.getAsset.output.code!= '0'}",        
            "output": {
                "code": "99",
                "message": "invalide asset"
        }


   

