## Expression Engine

The engine uses heavily templating and expression parsing to get the dynamic flow required by a generic workflow engine.

It uses the lib [ExpressionEngine](https://gitlab.readinessit.com/pbu/java-commons/commons-expression)
This lib extend
Spring [Expression Language (SpEL)](https://docs.spring.io/spring/docs/4.3.10.RELEASE/spring-framework-reference/html/expressions.html)
with some additional functionality. With suppoet for JSR223, xpath and jsonpath

With this way the core is implemented is possible to by only using configuration, map interfaces, parameters, templating
response, even use any bean that is available in the project

    "outputParameters": {
        "actionUrl": "${execution.task1.output.actionUrl}",
        "token": "${execution.task1.output.token}",
        "code": "${execution.task1.output.code}"
    }

#### Setting variables

    "statusResponse": "14",

#### Mapping variables

    "statusResponse": "${workflow.status}"

#### Using string and variables

     "code": "code is ${execution.task2.output.code}"
     "amount": "${context.inputs.workflow.amount}00" 

#### Using if-then-else

    "code": "code is ${execution.task3.output.code!=null?execution.task3.output.code:99}"
    "eventStatus": "${workflow.status==0?'ST3':'ST6'}"
    "description": "${workflow.status==0? workflow.amountTransactionResult == context.inputs.workflow.amount? 'PAGO CONFIRMADO' : 'MONTO INTEGRADOR INVALIDO': workflow.statusDescription}", 

#### String functions

    "transactionid": "${workflow.headers.transactionid.toString().substring(14,30)}"

#### Java function

    all java funcions can be used, but must use templating $(T(classe.func(param1,param2))
    
    "clientReqTimestamp": "${T(java.time.Instant).now()}",
	"eventID": "${T(java.util.UUID).randomUUID().toString().replaceAll('-', '')}",

#### Bean

    "beanExample": "${ @myBean(input)}

#### xpath

    xpath(script,var_int_the_context)
    
    responseCode: "${xpath('if (/methodResponse/params) then /methodResponse/params/param/value/struct/member[name=\"responseCode\"]/value/i4 else /methodResponse/fault/value/struct/member[name=\"faultCode\"]/value/i4','rawBody')}"   #"${xpath('/methodResponse/params/param/value/struct/member[name=\"responseCode\"]/value/i4','rawBody')}"
    balance: "${xpath('sum(/methodResponse/params/param/value/struct/member[name=\"dedicatedAccountInformation\"]/value/array/data/value/struct/member[name=\"dedicatedAccountValue1\"]/value/string)','rawBody')}"

#### jsonPath

json path function need two parameters query and context, the query it's an normal Json Path expression, and the context
can be and instance of Map or String(to use an instance of string this one need to be a valid json).

    jsonPath(query, context)

    responseMessage: "${jsonpath('$.response.message', output.body)}"

#### **JSR223** - standard scripting API for Java Virtual Machine

The JVM languages provide varying levels of support for the JSR223 API and interoperability with the Java runtime.

jsr233 was implemented in expressionEngine with the functions:

    eval(scriptEngine,script)
    invokeFunction(engine,script,function,paramenters)

Examples:

- JavaScript (nashorn) is built in jvm (deprecated in 11)

  engineName alias: nashorn/Nashorn/js/JS/JavaScript/javascript/ECMAScript/ecmascript

        "text": "${eval('JavaScript',\"var greeting='Hello '; for(var i=workflow.count;i>0;i--) { greeting+=workflow.name + ' '} greeting+=' gretings'\")}"
        "text": "${eval('JavaScript',\"Java.asJSONCompatible({ number: 42, greet: 'hello', primes: [2,3,5,7,11,13] })\")}"

  NOTE: nashord was deprecated in JDK11 (still has support until 2026, be careful when upgrade), the replacement is
  graalvm.js (and has more performance and newer js standard supported), it's possible to import graalvm.js to jdk11 (
  instead of using graalvm), but requires some tweaking to import (unless you use graalvm instead or jvm)
    - https://medium.com/graalvm/nashorn-removal-graalvm-to-the-rescue-d4da3605b6cb
    - https://medium.com/graalvm/graalvms-javascript-engine-on-jdk11-with-high-performance-3e79f968a819

#### Groovy

    engineName alias: groovy/Groovy      
        
        <!--add dependency to pom-->
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-jsr223</artifactId>
            <version>3.0.4</version>
        </dependency>

        "text": "${eval('groovy','workflow.name.toUpperCase()')}"    
        "text": "${invokeFunction('groovy','  def factorial(n) { n == 1 ? 1 : n * factorial(n - 1) } ','factorial',5).toString()}"     

#### Jshell

    - https://github.com/eobermuhlner/jshell-scriptengine

#### Other Engine.

    Many other engine are supported (python, ruby, etc), jsr223 is an interface, import de dependency in pom and specify engineName in eval or invokeFunction

### Advanced examples

