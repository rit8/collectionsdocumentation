# Task Definition

Parameter        | Description                                                       | Notes
---------------- | ------------------------------------------------------------------|-------- 
label            | identifier of the task in the system
component        |when defining a component it will use the taskHandler the has the task logic, the name/template will be ignored. | see below expected value
description      | Description of the task  |
type             | type of constructs to control the flow of execution| see below type of tasks|
failureWorkflowId| WorkflowId to be executed on the failure of the task| see more in [Failure](failure.md)
failureInputParameters| input parameters to send to the failure execution| see more in [Failure](failure.md)
failureOutputParameters | Same functionality of the failureOutputParameters included in the workflow definition but in this case the task have priority so if an task fail an have this parameter configured the engine will assume this as failure output |
retry            | number of retry in case of failure|
retryDelay       | delay between retrys|
inputParameters  | inputs parameters to sent to the task (dynamic and will change byt type of task)| see each task for the parameters
outputParameters | outputs parameters that are based based on the task execution output |

## Type of task

The type of tasks can be:

- system task
    - Control the execution flow
- worker tasks
    - component
    - templates

### Control of flow

This are special tasks that can control the flow execution, similar to if, switch, for in a programing language

List of control flow task available:

* [decision tasks](controlflowtasks/decisiontask.md)
* [logical task](controlflowtasks/logicaltask.md)
* [terminate task](controlflowtasks/terminatetask.md)
* [parallel tasks](controlflowtasks/paralleltask.md)
* [for tasks](docs/mkdocs/docs/configuration/controlflowtasks/fortask.md)
* subflow (in roadmap)
* each task (in roadmap)
* fork/join task (in roadmap)
* cancel tasks (in roadmap)
* extending (in roadmap)

### Worker tasks

* [simple tasks](workertasks/simpletask.md)
* [multistep tasks](workertasks/multisteptask.md)
* simpleThreaded tasks
* multiThreaded tasks

#### Components

* [http](taskhandler/http.md)
* [print](taskhandler/print.md)
* [jsonPath](taskhandler/jsonPath.md)
* [mergeListTask](taskhandler/MergeListTask.md)
* [Shell Script](taskhandler/shellscript.md)
* [extending](../extending/task.md)

#### Templates

user can register task template in the system.
