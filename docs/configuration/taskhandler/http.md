#### Http Request Task

![hexagonal](../../img/flow_simple_task.jpg)

Generic http task that will handle http requests to a remote server.

## Task Definition Parameters

Parameter                   | Required    | Type    |        Description                                           | Notes
--------------------------- | ------------|---------|--------------------------------------------------------------|-------- 
label                       | yes         | String  | unique name, key store the task info                         |
component                   | yes         | enum    | Unique name that it's associated with a registered function  | must be http
type                        | yes         | String  | simple (or other worker type task)                           |
rawRequest                  | no          | Boolean | true=send in string format, false=map body in json/xml in objects   |
errorHandler                | no          | Boolean | yes=will use internal error handling (http status code 4XX, 5XX will be treated has error and the flow will stop), no=will ignore http errors, it's up to flow to validate de result will logical/terminate tasks| exception like timeout will still raise an error
inputParameters             | yes         | Map     | list of parameter for the task                               | see below
inputParametersMapOptions   | no          | Map     | options mapping of the inputs                                | value can be null, origin or error, default: error
outputParameters            | no          | Map     | mapping of the response                                      |
outputParametersMapOptions  | no          | Map     | options mapping of the response                              | value can be null, origin or error, default: null
description                 | no          | String  | description                                                  |
successCondition            | no          | String  | Spel expression that will validate the result of the task    | see below
failureWorkflowId           | no          | String  | WorkflowId called on the failure of the task                 | see notes of the failure
failureInputParameters      | no          | Map     | Parameters added to the default context given on the failure | see notes of the failure

### Parameters - inputParameters

Parameter        | Required | Type    | Description                                                       | Notes
---------------- | ---------|---------|-------------------------------------------------------------------|-------- 
url              | yes      | String  | endpoint                                                          |
headers          | no       | Map     | http headers                                                      | default "application/json"
httpMethod       | yes      | String  | GET, POST, PUT, PATCH, DELETE, OPTIONS, ....                      |
body             | no       | Object  | it can be string, map, list  |                                    |

### Parameters -inputParametersMapOptions

options that can me used when mapping the parameters

Parameter                   | Type    | Description                                                                 | Notes
----------------------------|---------|-----------------------------------------------------------------------------|-------- 
returnOnEvaluationException | enum    | when evaluating the mapping exception (raise an error) ,null (empty value) , source (ignore mapping)                           | by default is exception, if we cannot map a parameter we sould not sent unmapped values to the server

### Parameters - outputParameters

most of the time, the response from the api is not in the format we need, or has more information needed, with
outputParameters it's possible to map the response and doing some validation

     outputParameters: {
          "errorCode": "${body.Body.GetCustomerAccount_Output.Error_spcCode}"
          "errorMessage": "${output.body.Body.GetCustomerAccount_Output.Error_spcMessage}"
          "contractNumber": "${output.body.Body.GetCustomerAccount_Output.ListOfAccountResponse.AccountResponse.DocumentNumber}"
          "contractType": "${output.body.Body.GetCustomerAccount_Output.ListOfAccountResponse.AccountResponse.DocumentType}"
          "accountStatus": "${output.body.Body.GetCustomerAccount_Output.ListOfAccountResponse.AccountResponse.AccountStatus}"
      }

## Parameters - outputParametersMapOptions

options that can me used when mapping the parameters

Parameter                   | Type    | Description                                                                 | Notes
----------------------------|---------|-----------------------------------------------------------------------------|-------- 
returnOnEvaluationException | enum    | when evaluating the mapping exception (raise an error) ,null (empty value) , source (ignore mapping)                           | by default is null

## Success Condition

On the use of the success condition the task will not raise error on the event of an HTTP CODE different from 2XX and
3XX, that is the default logic of the HTTP task, so instead the success condition will be analysed based on the
response, if the success condition return a boolean 'true' the task will be successful anything else the task will be
considered FAILED.

Bellow there is the output structure that will be used to construct the success condition:

Parameter        | Type    | Notes
-----------------|---------|--------
statusCode       | int                 |
body             | Map<String, Object> | available if the response it's a JSON
rawBody          | String              | available if the responste it's not a JSON
headers          | Map<String, String> |

### Response - output object

The http task will return an object with the name "output" will the following parameters:

Parameter        | Type    | Description                                                                 | Notes
---------------- |---------|-----------------------------------------------------------------------------|-------- 
statusCode       | int     | http status code                                                            |
headers          | Map     | object will all the http headers in the response                            |
body             | Map     | body contents (if any) transformed in Map                                   | by default or rawRequest=true
rawBody          | String  | body contents (if any) without object transformation (xml, json, soap)      | only when using rawRequest=true

### Response - Error object

the http task will return an object withe the name "error" when the task failed:

### Response - Error object - http error

Parameter        | Type    | Description                                                                 | Notes
---------------- |---------|-----------------------------------------------------------------------------|-------- 
message          | string  | http status code                                                            |
type             | string  | http                                                                        |
cause            | string  |                                                                             |
statusCode       | int     | http status code, (503 for timeouts)                                        |
httpResponse     | Map     | object when full http reponse                                               |

### Response - Error object - exception

Parameter        | Type    | Description                                                                 | Notes
---------------- |---------|-----------------------------------------------------------------------------|-------- 
message          | String  | exception message                                                           |
stackTrace       | String  | stack trace of the exception                                                |

### Example

    {
                "label": "transbankadaptor",
                "component": "Http",
                "type": "simple",
                "description": "test",
                "inputParameters": {
                	"url":"http://transbankadaptor-app:8283/api/web-pay-mall/init-transaction",
                	"headers": {
                		"Content-Type":"application/json"
                	},
                	"body": {
    	            	"buyOrder": "${workflow.buyOrder}",
    	            	"sessionId": "${workflow.sessionId}",
    	            	"returnUrl": "${workflow.returnUrl}", 
    	            	"finalUrl" : "${workflow.finalUrl}",
    	            	"stores": [{
    	            		"amount": "${workflow.amount}",
    	            		"commerceCode":"${workflow.commerce}",
    	            		"buyOrder": "${workflow.buyOrder}"
    	            	}]
                	}
                }          
            }
