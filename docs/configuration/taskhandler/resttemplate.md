## HTTP

**this task in deprecated, use http instead**

![hexagonal](../../img/flow_simple_task.jpg)

it will do a basic http request
the following example invokes a service, build a body with parameters thar are on workflow execution
since the execution is also a json dsl is possible to map any parameter if it is in context 

     {
                "label": "transbankadaptor",
                "name": "RestTemplate",
                "type": "simple",
                "description": "test",
                "inputParameters": {
                	"url":"http://transbankadaptor-app:8283/api/web-pay-mall/init-transaction",
                	"headers": {
                		"Content-Type":"application/json"
                	},
                	"body": {
    	            	"buyOrder": "${workflow.buyOrder}",
    	            	"sessionId": "${workflow.sessionId}",
    	            	"returnUrl": "${workflow.returnUrl}", 
    	            	"finalUrl" : "${workflow.finalUrl}",
    	            	"stores": [{
    	            		"amount": "${workflow.amount}",
    	            		"commerceCode":"${workflow.commerce}",
    	            		"buyOrder": "${workflow.buyOrder}"
    	            	}]
                	}
                }          
            }

**inputParameters**

Parameter        | Description                                                       | Notes
---------------- | ------------------------------------------------------------------|-------- 
url              | endpoint
headers          | http headers| 
body             | body in json format  | the body uses expression engine to map the parameters|
