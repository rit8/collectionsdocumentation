#### MultiStep Tasks

MultiStep tasks expand the simple task and add the possibility od add additional stages to the main task. **the multiStage is atomic**

- before
    - a step that can run tasks before de main task
- after
    - a strep that can run tasks after the main tasks    
- finally
    - a strep thar can run tasks in the end of the task block (even in a exception)
- exception
    - run tasks when and exception occurred.
    - this will not control an exception to each task (this was to be controlled in the taskHandler component), this is an exception of the all multiStage task
    - this is a mechanism to have an else in case the main block gets and exception.

example:

    {
            "type": "multiStage",
            "component": "Print",
            "label": "mainTask",
            "text": "mainTask - ${nome}",
            "before": [
                {
                    "component": "var",
                    "label": "nome",
                    "value": "nuno"
                },
                {
                    "component": "Print",
                    "label": "task130",
                    "text": "return setVariable before - ${nome}"
                }
            ],
            "after": [
                {
                    "component": "Print",
                    "label": "taska1",
                    "text": "mainTask - ${nome}"
                },
                {
                    "component": "var",
                    "label": "startInterval",
                    "value": 0
                },
                {
                    "component": "RandomInt",
                    "label": "taska3",
                    "startInterval": "1",
                    "endInterval": 1000
                }
            ],
            "finally": [
                {
                    "component": "RandomInt",
                    "label": "task130",
                    "startInterval": "${startInterval}",
                    "endInterval": 1000
                }
            ],
            "exception": [
                {
                    "component": "vars",
                    "label": "nome",
                    "values": {
                        "code": "99",
                        "description": "execption running task"
                    }
                },
                {
                    "component": "RandomInt",
                    "label": "taskend",
                    "startInterval": 0,
                    "endInterval": 1000
                }
            ]
    }
