# Simple Tasks

A simple task it's composed by a worker/executor, runs on the main thread and execute the task (component or template)
A simple task is ideal for fast and synchronous task. A simple task cannot be cancelled For long and control of the
execution, use the Threaded variance.

a simple task can be activated using type=simple

## Input Parameters & OutputParameters

In case of the task having inputParameters the executor will proceed with the evaluation by the expressionEngine, the
default implemented have the option of return different result in case of error (error, null and origin). This should be
configured in the inputParametersMapOptions.

All Tasks processed by the handler can have outputParameters this will create and easily accessed value by the rest of
the execution if necessary. For this we also have the option of configure the expressionEngine method of handling
errors, with the outputParametersMapOptions, this obey the same rules of the inputParametersMapOptions.

### Error

The expression engine will give an exception in case of no match found. This value is the default for the
inputParameters.

### Null

The expression engine will give will return null in case of no match found. This value is the default for the
outputParameters.

### Origin

The expression engine will give will return the expression in case of no match found.

## example

    {
        "type": "simple",
        "label": "actualizarFinEjecucion",
        "component": "RestTemplate",        
        "description": "test",
        "inputParametersMapOptions": "origin"
        "inputParameters": {
            "url": "http://logging-app:7286/services/logging/v1/generic/queue/Subscriptions/transactionId",
            "httpMethod": "PATCH",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": {
                "transactionId": "${workflow.headers.transactionId}",
                "endDate": "${T(java.time.Instant).now().toString()}"
            }
    }
