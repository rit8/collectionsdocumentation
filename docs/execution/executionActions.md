## Execution Actions

This part of the documentation will explain the actions that are possible to be done on the workflow

### Run

The action of run will start a flow with the inputs given on the body of the request.

path:
/v1/execution/workflow/{name}/run

### Retry

The action of run will rerun the action failed of the flow on the state FAILED if completed with success the flow will
continue.

path:
/v1/execution/workflow/{id}/retry

### Skip Task

Tha action of skip task will permit skipping and failed task of an FAILED workflow, permitting the execution to
continue.

path:
/workflow/{workflowId}/skiptask/{taskId}

### Resubmit

Tha action of resubmit will execute the same flow again with exactly the same inputs, this will be only available to
FAILED executions.

path:
/workflow/{workflowId}/resubmit

### Reject

Tha action of reject will put the execution in an REJECT state and execute the failure for the flow if we have any
configured

path:
/workflow/{workflowId}/reject
