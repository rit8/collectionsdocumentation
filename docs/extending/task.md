## Extending task

Tasks are the basic building blocks of a workflow. Each task has a type property which maps to a TaskHandler implementation, responsible for carrying out the task.

example the Print TaskHandler implementation:

    @Component(value = "Print")
    public class Print implements TaskHandler<Object> {
    
        private Logger log = LoggerFactory.getLogger(getClass());
    
        @Override
        public Object handle(Task task) {
            log.info(task.get("text"));
            //return null;
            return "xxxxx";
        }
    }

The TaskHandler is then responsible for executing the task using this input and optionally returning an output which can be used by other pipeline tasks downstream.

additional type of task can be added implementing the TaskHandler<?> interface
the name of the task in the workflow definition is determined by the registered bean `@Component(value = "Print")`
