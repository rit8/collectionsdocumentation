# Getting Started

## WorkFlow Definition

Workflows are defined using a JSON/YML based DSL and includes a set of tasks that are executed as part of the workflows.

    {
        "schemaVersion": 1,
        "version": 1,
        "restartable": false,
        "name": "webpayinit",
        "description": "webpayinit",
        "executionType": "sync",
        "workflowId": "test",
        "failureWorkflow": null,
        "tasks": [
            {
                "label": "transbankadaptor",
                "name": "RestTemplate",
                "type": "simple",
                "description": "test",
                "inputParameters": {
                    "url":"http://localhost:8283/api/web-pay-mall/init-transaction",
                    "headers": {
                        "Content-Type":"application/json"
                    },
                    "body": {
                        "buyOrder": "${workflow.buyOrder}",
                        "sessionId": "${workflow.sessionId}",
                        "returnUrl": "${workflow.returnUrl}", 
                        "finalUrl" : "${workflow.finalUrl}",
                        "stores": [{
                            "amount": "${workflow.amount}",
                            "commerceCode":"${workflow.commerce}",
                            "buyOrder": "${workflow.buyOrder}"
                        }]
                    }
                }
                ,
                "domain": "work"
            }
        ],
        "pushContext": {
            "stackId": "${execution.transbankadaptor.output.token}",
            "ttl": "84600",
            "stackParameters": {
                "inputs": "${inputs}",
                "token":"${execution.transbankadaptor.output.token}"
            }
        },
        "outputParameters": {
            "actionUrl":  "${execution.transbankadaptor.output.actionUrl}",
            "token": "${execution.transbankadaptor.output.token}",
            "code": "${execution.transbankadaptor.output.code}"
        }
    }


## Tasks
Tasks are the building blocks of Workflow. There must be at least one task in a Workflow.

     "tasks": [
          {
            "label": "init",
            "name": "Print",
            "type": "simple",
            "description": "test",
            "text": "test",
          }
        ],

 
## Workflow (Workflow Execution)
 
 A Workflow is the container of your process flow. 
 It could include several different types of Tasks, inputs and outputs connected to each other, to effectively achieve the desired result.

     {
            "outputs": {
                "code": "0",
                "actionUrl": "https://webpay3gint.transbank.cl/webpayserver/initTransaction",
                "token": "ef9fac53e9feff5fd599caeaaa71de8d25d91a8befa6a8e5cafd52de00baddb1"
            },
            "execution": {
                "transbankadaptor": {
                    "description": "test",
                    "label": "transbankadaptor",
                    "type": "simple",
                    "priority": 0,
                    "inputParameters": {
                        "headers": {
                            "Content-Type": "application/json"
                        },
                        "body": {
                            "stores": [
                                {
                                    "amount": "5000",
                                    "commerceCode": "597044444402",
                                    "buyOrder": "AAAAAAAAAAAA"
                                }
                            ],
                            "sessionId": "EEEEEEEEEEEE",
                            "returnUrl": "http://192.168.19.26:8283/api/web-pay-mall/transaction-result",
                            "buyOrder": "AAAAAAAAAAAA",
                            "finalUrl": "http://192.168.19.26:8283/api/web-pay-mall/transaction-finish"
                        },
                        "url": "http://transbankadaptor-app:8283/api/web-pay-mall/init-transaction"
                    },
                    "output": {
                        "code": "0",
                        "actionUrl": "https://webpay3gint.transbank.cl/webpayserver/initTransaction",
                        "token": "ef9fac53e9feff5fd599caeaaa71de8d25d91a8befa6a8e5cafd52de00baddb1"
                    },
                    "createTime": {
                        "nano": 349170000,
                        "epochSecond": 1565280244
                    },
                    "domain": "work",
                    "name": "RestTemplate",
                    "progress": 100,
                    "action": null,
                    "startTime": {
                        "nano": 349174000,
                        "epochSecond": 1565280244
                    },
                    "id": 1206,
                    "endTime": {
                        "nano": 334354000,
                        "epochSecond": 1565280246
                    },
                    "taskId": "9edff86a85b04dc896dccc514982d66e",
                    "workflowId": "0994fca63a654966876d1603c1e90225",
                    "status": "COMPLETED"
                }
            },
         "inputs": {
                    "workflow": {
                        "amount": "5000",
                        "channel": "WEB",
                        "sessionId": "EEEEEEEEEEEE",
                        "commerce": "597044444402",
                        "env": "PROD",
                        "market": "PP",
                        "createdDate": "2019-08-08T16:04:04.061117Z",
                        "msisdn": "56992382251",
                        "portal": "RECHARGE",
                        "returnUrl": "http://192.168.19.26:8283/api/web-pay-mall/transaction-result",
                        "buyOrder": "AAAAAAAAAAAA",
                        "finalUrl": "http://192.168.19.26:8283/api/web-pay-mall/transaction-finish",
                        "opOcc": "bla bla bla",
                        "workflowId": "webpayinit"
                    }
                },
                "currentTask": -1,
                "label": null,
                "priority": 0,
                "tags": [
                    ""
                ],
                "webhooks": null,
                "createTime": "2019-08-08T16:04:04.264320Z",
                "context": null,
                "startTime": "2019-08-08T16:04:04.325616Z",
                "id": 1156,
                "endTime": "2019-08-08T16:04:06.351086Z",
                "workflowId": "0994fca63a654966876d1603c1e90225",
                "status": "COMPLETED"
            },

## Start Workflow

send post to http://{{ip}}:{{port}}/workflow/{name}/run
body can have any parameter needed, the only restriction is that must be json (other formats although is possible by extending the api they are considered)
    
    POST http://{{ip}}:{{port}}/workflow/webpayinit/run
    
    {
        "commerce":"597044444402",
        "contextPath":"https://portalcautivo.entel.cl/",
        "market":"PP",
        "env":"PROD",
        "buyOrder":"AAAAAAAAAAAA",
        "sessionId":"EEEEEEEEEEEE",
        "msisdn":"56912345678",
        "amount":"5000",
        "opOcc":"",
        "opUrl":"",
        "portal":"RECHARGE",
        "channel":"WEB",
        "provider":"TRANSBANK",
        "returnUrl":"http://www.entel.cl/webpaytest/101/PROD/ok.iws",
        "finalUrl":"http://www.entel.cl/webpaytest/101/PROD/nok.iws"
    }
